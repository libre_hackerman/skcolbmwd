/*
 * Copyright (C) 2023 Esteban López Rodríguez <gnu_stallman@protonmail.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <skcolbmwd.h>

/*
 * Skips every char in f until a newline or EOF is found.
 */
static void
skip_line(FILE *f)
{
	char c;

	while ((c = fgetc(f)) != EOF && c != '\n');
}

/*
 * Seeks for a /proc/meminfo field and returns its number of kB.
 * Returns -1 if it failed to find the field.
 */
static long
seek_field(FILE *f, char *name)
{
	char buffer[25];
	long kb;
	int trials;

	kb = -1;
	trials = 0;
	buffer[0] = '\0';
	while (strcmp(buffer, name) && trials++ < 70)
	{
		fscanf(f, "%[^:]s", buffer);
		fscanf(f, ": %ld kB\n", &kb);
	}

	return (kb);
}

/*
 * Stores in ram_mb the used RAM in MB. The result is calculated doing:
 * MemTotal - MemFree - Buffers - Cached - SReclaimable + Shmem
 * Returns true if no errors happen.
 */
static bool
used_ram(unsigned int *ram_mb, unsigned int *swap_mb)
{
	FILE *f;
	long mtotal, mfree, mbuffers, mcached, msreclaimable, mshmem;
	long mswap_total, mswap_free;

	if (!(f = fopen("/proc/meminfo", "r")))
	{
		perror("Can't read /proc/meminfo");
		return (false);
	}

	fscanf(f, "MemTotal: %ld kB\n", &mtotal);
	fscanf(f, "MemFree: %ld kB\n", &mfree);
	skip_line(f);
	fscanf(f, "Buffers: %ld kB\n", &mbuffers);
	fscanf(f, "Cached: %ld kB\n", &mcached);
	mswap_total = seek_field(f, "SwapTotal");
	mswap_free = seek_field(f, "SwapFree");
	mshmem = seek_field(f, "Shmem");
	msreclaimable = seek_field(f, "SReclaimable");

	fclose(f);

	/* Check all fields correctly read */
	if (mswap_total == -1 || mswap_free == -1 || mshmem == -1 ||
			msreclaimable == -1)
	{
		fprintf(stderr, "Couldn't retrieve data from /proc/meminfo\n");
		return (false);
	}

	*ram_mb = (mtotal - mfree - mbuffers - mcached - msreclaimable + mshmem)
			/ 1024;
	*swap_mb = (mswap_total - mswap_free) / 1024;
	return (true);
}

/*
 * Calculates the bytes required for the output string.
 */
static size_t
calculate_output_len(unsigned int ram_mb, unsigned int swap_mb)
{
	size_t output_len;

	/* Calculate output string size */
	/* RAM, in either MB or GB */
	if (ram_mb < 1024)
		output_len = snprintf(NULL, 0, "%dM", ram_mb);
	else
		output_len = snprintf(NULL, 0, "%.3gG", ram_mb / 1024.0);
	/* Swap, if > 0 */
	if (swap_mb)
	{
		if (swap_mb < 1024)
			output_len += snprintf(NULL, 0, " + %dM", swap_mb);
		else
			output_len += snprintf(NULL, 0, " + %.3gG", swap_mb / 1024.0);
	}

	return (output_len);
}

/*
 * Allocates an string with the used RAM and swap in MB/GB
 * returning it. Returns NULL if it fails.
 */
char *
get_used_ram()
{
	char *output;
	unsigned int ram_mb, swap_mb;

	/* Retrieve used ram and swap */
	if (!used_ram(&ram_mb, &swap_mb))
		return NULL;

	/* Allocate output string */
	if (!(output = calloc(calculate_output_len(ram_mb, swap_mb) + 1, sizeof(char))))
	{
		perror("Can't allocate memory for used_mem");
		return NULL;
	}

	/* Copy data into *output */
	/* RAM */
	if (ram_mb < 1024)
		sprintf(output, "%dM", ram_mb);
	else
		sprintf(output, "%.3gG", ram_mb / 1024.0);
	/* Swap */
	if (swap_mb)
	{
		if (swap_mb < 1024)
			sprintf(output + strlen(output), " + %dM", swap_mb);
		else
			sprintf(output + strlen(output), " + %.3gG", swap_mb / 1024.0);
	}

	return output;
}
