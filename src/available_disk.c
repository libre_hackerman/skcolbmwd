/*
 * Copyright (C) 2023 Esteban López Rodríguez <gnu_stallman@protonmail.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <stdio.h>
#include <sys/statvfs.h>

#include <skcolbmwd.h>

#define bytes_to_mb(bytes) ((unsigned int)(bytes / 1024.0 / 1024.0))
#define bytes_to_gb(bytes) ((unsigned int)(bytes / 1024.0 / 1024.0 / 1024.0))
#define BYTES_IN_GB 1073741824

/*
 * Allocates an string with the current free disk in the mnt mountpoint in
 * megabytes or gigabytes. Returns NULL if it fails.
 */
char *
get_available_disk(const char *mnt)
{
	char *output;
	struct statvfs sf;
	unsigned long bytes;
	unsigned int mb, gb;
	bool use_gb;

	/* Retrieve filesystem information */
	if (statvfs(mnt, &sf) == -1)
	{
		perror("Can't get filesystem information");
		return NULL;
	}

	/* Calculate available bytes, megabytes and gigabytes */
	bytes = sf.f_bavail * sf.f_bsize;
	mb = bytes_to_mb(bytes);
	gb = bytes_to_gb(bytes);
	use_gb = bytes >= BYTES_IN_GB;

	/* Allocate output string */
	if (!(output = calloc(snprintf(NULL, 0, "%d?", use_gb ? gb : mb) + 1,
			sizeof(char))))
	{
		perror("Can't allocate available disk output");
		return NULL;
	}

	/* Store data into output string */
	sprintf(output, "%d%c", use_gb ? gb : mb, use_gb ? 'G' : 'M');

	return output;
}
