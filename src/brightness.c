/*
 * Copyright (C) 2023 Esteban López Rodríguez <gnu_stallman@protonmail.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include <skcolbmwd.h>
#include <util.h>

/*
 * Calculates the percentage of brightness given the backlight entry name.
 * Returns -1 in case of error.
 */
static int
calculate_brightness(const char *backlight)
{
	char *current_name, *max_name;
	long current, max;

	/* Get current brightness filename */
	if (!(current_name = calloc(32 + strlen(backlight) + 1, sizeof(char))))
	{
		perror("Can't allocate backlight filename");
		return (-1);
	}
	sprintf(current_name, "/sys/class/backlight/%s/brightness", backlight);

	/* Get max brightness filename */
	if (!(max_name = calloc(36 + strlen(backlight) + 1, sizeof(char))))
	{
		perror("Can't allocate max backlight filename");
		return (-1);
	}
	sprintf(max_name, "/sys/class/backlight/%s/max_brightness", backlight);

	/* Read files */
	current = read_number_file(current_name);
	max = read_number_file(max_name);

	/* Free filenames */
	free(current_name);
	free(max_name);

	/* Check for errors */
	if (current == -1 || max == -1)
		return (-1);

	/* Calculate percentage and return it */
	return (current * 100 / max);
}

/*
 * Allocates an string with the current brightness in the specified
 * /sys/class/backlight/ entry. Returns NULL if it fails.
 */
char *
get_brightness(const char *backlight)
{
	char *output;
	int brightness;

	/* Get brightness */
	if ((brightness = calculate_brightness(backlight)) == -1)
		return NULL;

	/* Allocate output string */
	if (!(output = calloc(snprintf(NULL, 0, "%d%%", brightness) + 1, sizeof(char))))
	{
		perror("Can't allocate brightness output");
		return NULL;
	}

	/* Write brightness to the output string */
	sprintf(output, "%d%%", brightness);

	return output;
}
