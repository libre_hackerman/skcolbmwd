/*
 * Copyright (C) 2023 Esteban López Rodríguez <gnu_stallman@protonmail.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <stdio.h>
#include <sys/time.h>
#include <string.h>

#include <skcolbmwd.h>
#include <util.h>

#define BYTES_IN_MB 1048576
#define bytes_to_kb(bytes) ((bytes) / 1024.0)
#define bytes_to_mb(bytes) ((bytes) / 1024.0 / 1024.0)

/*
 * Measures traffic speed on the specified interface and stores it in
 * up and down, in bytes/second. Stores -1 in both if there's an error.
 */
static void
get_bytes_per_second(const char *interface, long *up, long *down)
{
	static long up_prev, down_prev;
	static struct timeval time_prev;

	char *tx_file, *rx_file;
	long up_now, down_now;
	struct timeval time_now;
	double seconds_diff;

	/* Allocate filenames to read up/down bytes from */
	if (!(rx_file = calloc(strlen(interface) + 36, sizeof(char))))
	{
		perror("Can't allocate rx_bytes filename");
		*up = *down = -1;
		return;
	}
	sprintf(rx_file, "/sys/class/net/%s/statistics/rx_bytes", interface);

	if (!(tx_file = calloc(strlen(interface) + 36, sizeof(char))))
	{
		perror("Can't allocate tx_bytes filename");
		*up = *down = -1;
		free(rx_file);
		return;
	}
	sprintf(tx_file, "/sys/class/net/%s/statistics/tx_bytes", interface);

	down_now = read_number_file(rx_file);
	up_now = read_number_file(tx_file);
	gettimeofday(&time_now, NULL);

	/* Free filenames */
	free(rx_file);
	free(tx_file);

	/* Check for errors */
	if (down_prev == -1 || up_prev == -1 || down_now == -1 || up_now == -1)
	{
		*up = *down = -1;
		return;
	}

	/* Stores the bytes increment in half second x2 to get in one second */
	seconds_diff = time_now.tv_sec - time_prev.tv_sec +
		(time_now.tv_usec - time_prev.tv_usec) * 0.000001;
	*down = (down_now - down_prev) / seconds_diff;
	*up = (up_now - up_prev) / seconds_diff;

	// Store current values for next call
	up_prev = up_now;
	down_prev = down_now;
	time_prev = time_now;
}

/*
 * Allocates an string with the current network speed on the interface
 * specified. Returns NULL if it fails.
 */
static char *
get_net_speed_interface(const char *interface)
{
	char *output;
	long bytes_up, bytes_down;
	bool up_mb, down_mb;
	double up, down;

	/* Retrieve speeds */
	get_bytes_per_second(interface, &bytes_up, &bytes_down);
	if (bytes_up == -1 && bytes_down == -1)
		return NULL;

	/* Convert bytes to KB/MB */
	up_mb = bytes_up >= BYTES_IN_MB;
	down_mb = bytes_down >= BYTES_IN_MB;
	up = up_mb ? bytes_to_mb(bytes_up) : bytes_to_kb(bytes_up);
	down = down_mb ? bytes_to_mb(bytes_down) : bytes_to_kb(bytes_down);

	/* Allocate output string */
	if (!(output = calloc(snprintf(NULL, 0, "D: %.2f ? U: %.2f ?", down, up) + 1,
			sizeof(char))))
	{
		perror("Can't allocate network speed output string");
		return NULL;
	}

	/* Write speeds to output string */
	sprintf(output, "D: %.2f %c U: %.2f %c", down, down_mb ? 'M' : 'K',
			up, up_mb ? 'M' : 'K');

	return output;
}

static bool
is_interface_up(const char *interface)
{
	char *operstate_file;
	char state[3];
	FILE *f;

	/* Allocate filename to read interface state */
	if (!(operstate_file = calloc(strlen(interface) + 26, sizeof(char))))
	{
		perror("Can't allocate operstate filename");
		return (false);
	}
	sprintf(operstate_file, "/sys/class/net/%s/operstate", interface);
	f = fopen(operstate_file, "r");
	free(operstate_file);
	if (!f)
		return (false);
	fread(state, sizeof(char), 2, f);
	state[2] = '\0';
	fclose(f);
	return (strcmp(state, "up") == 0);
}

/*
 * Allocates an string with the current network speed on the first up interface
 * specified in the NULL-terminated net_interfaces array.
 * Returns NULL if it fails.
 */
char *
get_net_speed(char *const *net_interfaces)
{
	while (*net_interfaces && !is_interface_up(*net_interfaces))
		net_interfaces++;

	if (*net_interfaces)
		return get_net_speed_interface(*net_interfaces);
	else
		return strdup("no network");
}
