/*
 * Copyright (C) 2023 Esteban López Rodríguez <gnu_stallman@protonmail.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <util.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <dirent.h>

/*
 * Appends src to dst, by freeing the previous dst and allocating the new one.
 * Returns true upon success.
 */
bool
strjoin(char **dst, const char *src)
{
	char *new;

	if (!(new = calloc((*dst ? strlen(*dst) : 0) + strlen(src) + 1,
			sizeof(char))))
	{
		perror("Can't allocate new string in append_char");
		return (false);
	}

	if (*dst)
		strcat(new, *dst);
	strcat(new, src);

	free(*dst);
	*dst = new;
	return (true);
}

/*
 * Returns true if dir_path is a readable directory.
 */
bool
dir_readable(char *dir_path)
{
	DIR *dir;

	if ((dir = opendir(dir_path)))
	{
		closedir(dir);
		return (true);
	}
	return (false);
}

/*
 * Reads a long integer from a file that ONLY has that number.
 * Returns -1 in case of error.
 */
long
read_number_file(const char *filename)
{
	char buffer[22];
	FILE *f;

	/* Read number from file */
	if (!(f = fopen(filename, "r")))
	{
		perror(filename);
		return (-1);
	}
	fgets(buffer, 22, f);
	fclose(f);

	return (atol(buffer));
}
