/*
 * Copyright (C) 2023 Esteban López Rodríguez <gnu_stallman@protonmail.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <skcolbmwd.h>
#include <util.h>

/*
 * Allocates an string with the output command returning it.
 * Returns NULL if it fails.
 */
char *
get_command(const char *command)
{
	char *output = NULL;
	FILE *stream;
	char buffer[15];
	int exit_status;
	size_t last_char_i;

	/* Launch command */
	if (!(stream = popen(command, "r")))
	{
		perror("Error opening process for command");
		fprintf(stderr, "Command: %s\n", command);
		return NULL;
	}

	/* Read output */
	strjoin(&output, ""); /* So empty outputs doesn't result in NULL */
	while (fgets(buffer, 15, stream))
	{
		if (!strjoin(&output, buffer))
		{
			fprintf(stderr, "Can't allocate memory for command's output\n");
			free(output);
			pclose(stream);
			return NULL;
		}
	}
	/* Remove terminating newline if there's one */
	if (output[last_char_i = (strlen(output) - 1)] == '\n')
		output[last_char_i] = '\0';

	/* Exit status warning */
	if ((exit_status = pclose(stream)))
	{
		fprintf(stderr, "Warning: %s\nExited with status %d\n", command,
			exit_status);
	}

	return output;
}
