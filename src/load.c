/*
 * Copyright (C) 2023 Esteban López Rodríguez <gnu_stallman@protonmail.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <skcolbmwd.h>

/*
 * Allocates an string with the current load in n_avgs averages (1 to 3).
 * Returns NULL if it fails.
 */
char *
get_load(const char *n_avgs)
{
	char *output;
	int n_avgs_int;
	double avgs[3];
	size_t len;

	/* Get the number of averages and check is in [1, 3] */
	n_avgs_int = atoi(n_avgs);
	if (n_avgs_int < 1 || n_avgs_int > 3)
		return NULL;

	/* Retrieve averages */
	getloadavg(avgs, n_avgs_int);

	/* Calculate output length */
	len = 0;
	for (int i = 0; i < n_avgs_int; i++)
		len += snprintf(NULL, 0, "%.2f ", avgs[i]);

	/* Allocate output string */
	if (!(output = calloc(len + 1, sizeof(char))))
	{
		perror("Can't allocate load averages output");
		return NULL;
	}

	/* Store load averages */
	for (int i = 0; i < n_avgs_int; i++)
		sprintf(output + strlen(output), "%.2f ", avgs[i]);

	/* Replace last space with \0 terminator */
	output[len - 1] = '\0';

	return output;
}
