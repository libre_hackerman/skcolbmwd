/*
 * Copyright (C) 2023 Esteban López Rodríguez <gnu_stallman@protonmail.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <stdio.h>
#include <signal.h>
#include <X11/Xlib.h>
#include <unistd.h>
#include <time.h>
#include <string.h>
#include <sys/wait.h>

#include <skcolbmwd.h>
#include "../config.h"

#define get_root_window(dpy) RootWindow(dpy, DefaultScreen(dpy))

typedef enum {DWM, STDOUT, TMUX} output_t;

/* Global variables */
static size_t n_blocks = sizeof(blocks) / sizeof(block_t);
static block_status_t *blocks_status = NULL;
static char **blocks_outputs = NULL;
static Display *dpy = NULL;
static char **net_interfaces = NULL;

/*
 * Frees all resources and exits
 */
static void
free_everything()
{
	free(blocks_status);

	if (blocks_outputs)
	{
		for (size_t i = 0; i < n_blocks; i++)
			free(blocks_outputs[i]);
		free(blocks_outputs);
	}

	if (net_interfaces)
	{
		for (size_t i = 0; net_interfaces[i]; i++)
			free(net_interfaces[i]);
		free(net_interfaces);
	}

	if (dpy)
		XCloseDisplay(dpy);
}

/*
 * Allocates blocks_status and sets its length to n_blocks. signal_recv is
 * set to true in all of them to do the first initialization.
 */
static void
init_blocks_status()
{
	if (!(blocks_status = calloc(n_blocks, sizeof(block_status_t))))
	{
		perror("Can't allocate blocks_status");
		free_everything();
		exit(EXIT_FAILURE);
	}

	for (size_t i = 0; i < n_blocks; i++)
	{
		blocks_status[i].last_updated = 0;
		blocks_status[i].signal_recv = true;
	}
}

/*
 * Allocates blocks_outputs and sets its length to n_blocks. Sets each
 * element to NULL.
 */
static void
init_blocks_outputs()
{
	if (!(blocks_outputs = calloc(n_blocks, sizeof(char*))))
	{
		perror("Can't allocate blocks_outputs");
		free_everything();
		exit(EXIT_FAILURE);
	}

	for (size_t i = 0; i < n_blocks; i++)
		blocks_outputs[i] = NULL;
}

/*
 * Initializes dpy (X11 Display)
 */
static void
init_display()
{
	if (!(dpy = XOpenDisplay(NULL)))
	{
		perror("Can't open X11 Display");
		free_everything();
		exit(EXIT_FAILURE);
	}
}

/*
 * Signal handler for closing
 */
static void
free_and_close(int sig)
{
	psignal(sig, "Received signal");
	free_everything();
	exit(EXIT_SUCCESS);
}

/*
 * Signal handler that identifies the received signal and sets to true
 * the signal_recv field of the pertient blocks_status elements.
 */
static void
signal_for_block(int sig)
{
	for (size_t i = 0; i < n_blocks; i++)
	{
		if (blocks[i].signal == sig - SIGRTMIN)
			blocks_status[i].signal_recv = true;
	}
}

/*
 * Searches for the argument of the NET_SPEED block containing the net
 * interfaces and sets the net_interfaces global variable to a NULL terminated
 * array of strings with one interface in each array element.
 * Returns NULL if the NET_SPEED block is not set.
 */
static void
parse_net_interfaces()
{
	char *raw_interfaces;
	size_t n_inter;
	char *interface;
	bool first;

	/* Get a copy of the block argument */
	net_interfaces = NULL;
	raw_interfaces = NULL;
	for (size_t i = 0; i < n_blocks && !raw_interfaces; i++)
		if (blocks[i].type == NET_SPEED)
			raw_interfaces = strdup(blocks[i].argument);
	if (!raw_interfaces)
		return;

	n_inter = 0;
	first = true;
	do
	{
		interface = strtok(first ? raw_interfaces : NULL, " ");
		first = false;
		net_interfaces = reallocarray(net_interfaces, ++n_inter, sizeof(char*));
		net_interfaces[n_inter - 1] = interface ? strdup(interface) : NULL;
	}
	while (interface);
	free(raw_interfaces);
}

/*
 * Sums alls block_outputs into one string.
 */
static char *
merge_blocks_outputs()
{
	char *merge;
	size_t merge_len;

	merge_len = 1;  /* Terminating \0 */
	for (size_t i = 0; i < n_blocks; i++)
		merge_len += strlen(blocks_outputs[i]);
	if (!(merge = calloc(merge_len, sizeof(char))))
	{
		perror("Can't allocate merge");
		free_everything();
		exit(EXIT_FAILURE);
	}
	for (size_t i = 0; i < n_blocks; i++)
		strcat(merge, blocks_outputs[i]);

	return (merge);
}

/*
 * Prints the statusbar string to stdout
 */
static void
show_statusbar_stdout()
{
	char *string;

	string = merge_blocks_outputs();
	puts(string);
	fflush(stdout);
	free(string);
}

/*
 * Set the statusbar string as Tmux right status
 */
static void
set_statusbar_tmux()
{
	char *string;
	int pid, status;

	string = merge_blocks_outputs();
	switch ((pid = fork()))
	{
		case -1:
			perror("Can't set Tmux status");
			free(string);
			free_everything();
			exit(EXIT_FAILURE);
			break;
		case 0:
			execlp("tmux", "tmux", "set", "-g", "status-right", string, NULL);
			break;
		default:
			waitpid(pid, &status, 0);
			free(string);
			break;
	}
}

/*
 * Update statusbar string with the sum of blocks_outputs.
 */
static void
update_statusbar()
{
	char *new_string;
	char *current_string;

	new_string = merge_blocks_outputs();

	/* Get current statusbar string */
	XFetchName(dpy, get_root_window(dpy), &current_string);

	/* Update statusbar if string changed */
	if (!current_string || strcmp(new_string, current_string))
	{
		XStoreName(dpy, get_root_window(dpy), new_string);
		XSync(dpy, False);
	}

	free(new_string);
	XFree(current_string);
}

/*
 * Adds the label and/or separator to blocks_outputs[i] if required according
 * to blocks[i]. Doesn't add anything if blocks_outputs[i] is empty.
 */
static void
add_label_separator(size_t i)
{
	size_t new_len;
	size_t label_len;
	char *old_output;

	if (!strlen(blocks_outputs[i]))
		return;

	/* Calculate size of label and separator if required */
	new_len = 0;
	if ((label_len = strlen(blocks[i].label)))
		new_len += label_len + 1;
	if (blocks[i].separator)
		new_len += 3;
	else
		new_len++;

	/* Allocate new output */
	new_len += strlen(blocks_outputs[i]);
	old_output = blocks_outputs[i];
	if (!(blocks_outputs[i] = calloc(new_len + 1, sizeof(char))))
	{
		perror("Can't allocate label and/or separator");
		blocks_outputs[i] = old_output;
		free_everything();
		exit(EXIT_FAILURE);
	}

	/* Append label if required */
	if (label_len)
		sprintf(blocks_outputs[i], "%s ", blocks[i].label);
	/* Append old output */
	strcat(blocks_outputs[i], old_output);
	/* Append separator if required */
	if (blocks[i].separator)
	{
		sprintf(blocks_outputs[i] + strlen(blocks_outputs[i]), " %c ",
				separator);
	}
	else
		strcat(blocks_outputs[i], " ");

	/* Free old output */
	free(old_output);
}

/*
 * Infinite loop that will be checking and updating the statusbar.
 * If stdout_output is different of 0, it will print the statusbar string to
 * stdout instead of setting it as the root window name.
 */
static void
mainloop(output_t output)
{
	time_t now;

	while (true)
	{
		now = time(NULL);

		for (size_t i = 0; i < n_blocks; i++)
		{
			if (blocks_status[i].signal_recv || (blocks[i].delay &&
					now - blocks_status[i].last_updated >= blocks[i].delay))
			{
				/* Free previous string */
				free(blocks_outputs[i]);
				blocks_outputs[i] = NULL;

				/* Update block output */
				switch (blocks[i].type)
				{
					case COMMAND:
						blocks_outputs[i] = get_command(blocks[i].argument);
						break;
					case TIME:
						blocks_outputs[i] = get_time(blocks[i].argument);
						break;
					case USED_RAM:
						blocks_outputs[i] = get_used_ram();
						break;
					case NET_SPEED:
						blocks_outputs[i] = get_net_speed(net_interfaces);
						break;
					case BATTERY:
						blocks_outputs[i] = get_battery(blocks[i].argument);
						break;
					case BATTERY_NO_TIME:
						blocks_outputs[i] = get_battery_no_time(blocks[i].argument);
						break;
					case LOAD:
						blocks_outputs[i] = get_load(blocks[i].argument);
						break;
					case AVAILABLE_DISK:
						blocks_outputs[i] = get_available_disk(blocks[i].argument);
						break;
					case BRIGHTNESS:
						blocks_outputs[i] = get_brightness(blocks[i].argument);
						break;
					default:
						fputs("Invalid block\n", stderr);
						free_everything();
						exit(EXIT_FAILURE);
				}

				/* Add label and separator if required */
				if (blocks_outputs[i])
					add_label_separator(i);
				else /* Exit if the output is NULL */
				{
					free_everything();
					exit(EXIT_FAILURE);
				}

				/* Update block status */
				blocks_status[i].last_updated = now;
				blocks_status[i].signal_recv = false;
			}
		}

		switch (output)
		{
			case DWM:
				update_statusbar();
				break;
			case TMUX:
				set_statusbar_tmux();
				break;
			case STDOUT:
				show_statusbar_stdout();
				break;
		}

		sleep(1);
	}
}

int
main(int argc, char *argv[])
{
	output_t output;

	/* Parse output argument */
	if (argc == 2 && strcmp(argv[1], "--stdout") == 0)
		output = STDOUT;
	else if (argc == 2 && strcmp(argv[1], "--tmux") == 0)
		output = TMUX;
	else
		output = DWM;

	/* Initialize blocks and Display */
	init_blocks_status();
	init_blocks_outputs();
	if (output == DWM)
		init_display();

	/* Set the net_interfaces array */
	parse_net_interfaces();

	/* Set close handler */
	signal(SIGTERM, free_and_close);
	signal(SIGINT, free_and_close);

	/* Assign signal handler to each block that requires it */
	for (size_t i = 0; i < n_blocks; i++)
	{
		if (blocks[i].signal != -1)
			signal(SIGRTMIN + blocks[i].signal, signal_for_block);
	}

	mainloop(output);

	return (EXIT_FAILURE);
}
