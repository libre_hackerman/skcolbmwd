/*
 * Copyright (C) 2023 Esteban López Rodríguez <gnu_stallman@protonmail.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

#include <skcolbmwd.h>

#define MAX_TIME_STR 60

/*
 * Allocates an string with the current time formatted returning it.
 * Returns NULL if it fails.
 */
char *
get_time(const char *format)
{
	time_t now;
	struct tm *tm;
	char formatted_time[MAX_TIME_STR];

	/* Get formatted time */
	now = time(NULL);
	if (!(tm = localtime(&now)))
	{
		perror("Can't get localtime");
		return NULL;
	}
	strftime(formatted_time, MAX_TIME_STR, format, tm);

	return strdup(formatted_time);
}
