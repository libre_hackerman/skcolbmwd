/*
 * Copyright (C) 2023 Esteban López Rodríguez <gnu_stallman@protonmail.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#include <skcolbmwd.h>
#include <util.h>

typedef enum {CHARGING, DISCHARGING, FULL, UNKNOWN} batt_status_t;

/*
 * This function reads a maximum of max characters from directory/filename and
 * stores them into dst, returning true if success.
 */
static bool
read_file(char *directory, char *filename, char *dst, int max)
{
	char *path;
	FILE *f;

	/* Compose file path */
	if (!(path = calloc(strlen(directory) + strlen(filename) + 2, sizeof(char))))
	{
		perror("Can't allocate memory for file path");
		return (false);
	}
	sprintf(path, "%s/%s", directory, filename);

	/* Open file for reading */
	if (!(f = fopen(path, "r")))
	{
		perror(path);
		free(path);
		return (false);
	}

	/* Read from f into dst */
	fgets(dst, max, f);

	fclose(f);
	free(path);
	return (true);
}

/*
 * Returns the status of the battery given its /sys directory.
 */
static batt_status_t
read_battery_status(char *dir_batt_path)
{
	char buffer[15];

	/* Read battery status */
	if (!read_file(dir_batt_path, "status", buffer, 15))
		return (UNKNOWN);

	if (strcmp(buffer, "Charging\n") == 0)
		return (CHARGING);
	else if (strcmp(buffer, "Discharging\n") == 0)
		return (DISCHARGING);
	else if (strcmp(buffer, "Full\n") == 0)
		return (FULL);
	else
		return (UNKNOWN);
}

/*
 * Returns the battery capacity level given its /sys directory.
 * Return -1 if the information couldn't be retrieved.
 */
static int
read_capacity(char *dir_batt_path)
{
	char buffer[4];

	/* Read capacity */
	if (!read_file(dir_batt_path, "capacity", buffer, 4))
		return (-1);

	return (atoi(buffer));
}

/*
 * Returns an allocated string with the remaining time for full charge/dischage
 * (depending on status) in hours:minutes format. Returns NULL if the status
 * is FULL/UNKNOWN or there was an error.
 */
static char *
read_battery_time(char *dir_batt_path, batt_status_t status)
{
	char buffer[10];
	int charge_full, charge_now, current_now;
	double hours_double;
	int hours, minutes;
	char *time;

	/* Check that the battery is charging/discharging */
	if (status != CHARGING && status != DISCHARGING)
		return (NULL);

	/* Read charge full */
	if (!read_file(dir_batt_path, "charge_full", buffer, 10))
		return (NULL);
	charge_full = atoi(buffer);

	/* Read charge now */
	if (!read_file(dir_batt_path, "charge_now", buffer, 10))
		return (NULL);
	charge_now = atoi(buffer);

	/* Read current now */
	if (!read_file(dir_batt_path, "current_now", buffer, 10))
		return (NULL);
	current_now = atoi(buffer);

	/* Calculate time */
	if (status == CHARGING)
		hours_double = 1.0 * (charge_full - charge_now) / current_now;
	else
		hours_double = 1.0 * charge_now / current_now;

	/* Extract hours and minutes */
	hours = (int) hours_double;
	minutes = (hours_double - hours) * 60;

	/* Store time into hours:minutes string */
	if (!(time = calloc(snprintf(NULL, 0, "%d:%.2d", hours, minutes) + 1, sizeof(char))))
	{
		perror("Can't allocate battery time");
		return (NULL);
	}
	sprintf(time, "%d:%.2d", hours, minutes);

	return (time);
}

/*
 * Allocates an string with the battery charge, status and the remaining time
 * to full charge/discharge returning it. Returns NULL if it fails.
 */
static char *
compose_battery(batt_status_t status, int capacity, char *time)
{
	char *output;
	size_t len;

	/* Calculate size and allocate output string */
	len = snprintf(NULL, 0, "%d%%", capacity);
	if (time && (status == CHARGING || status == DISCHARGING))
		len += strlen(time) + 5;  /* 4 comes from (,), , ,C/D */

	if (!(output = calloc(len + 1, sizeof(char))))
	{
		perror("Can't allocate battery data");
		return NULL;
	}

	/* Store output string */
	if (status == CHARGING || status == DISCHARGING)
	{
		if (time)
			sprintf(output, "%c %d%% (%s)", status == CHARGING ? 'C' : 'D',
				capacity, time);
		else
			sprintf(output, "%c %d%%", status == CHARGING ? 'C' : 'D',
				capacity);
	}
	else
		sprintf(output, "%d%%", capacity);

	return output;
}

/*
 * Allocates an string with the battery (specified in block->argument) charge,
 * status and the remaining time to full charge/discharge returning it.
 * Returns NULL if it fails.
 */
char *
get_battery(const char *battery_name)
{
	char *output = NULL;
	char *dir_batt_path;
	batt_status_t status;
	int capacity;
	char *time;

	/* Compose battery directory */
	if (!(dir_batt_path = calloc(strlen(battery_name) + 25, sizeof(char))))
	{
		perror("Can't allocate battery info");
		return NULL;
	}
	sprintf(dir_batt_path, "/sys/class/power_supply/%s", battery_name);

	/* If battery directory doesn't exist, store in output "-" */
	if (!dir_readable(dir_batt_path))
	{
		if (!(output = strdup("-")))
			perror("Can't allocate battery info");
		free(dir_batt_path);
		return output;
	}

	/* Get battery status */
	status = read_battery_status(dir_batt_path);

	/* Get battery capacity */
	if ((capacity = read_capacity(dir_batt_path)) == -1)
	{
		free(dir_batt_path);
		return NULL;
	}

	/* If charging or discharging, get time to completion */
	time = NULL;
	if (status == CHARGING || status == DISCHARGING)
	{
		if (!(time = read_battery_time(dir_batt_path, status)))
		{
			free(dir_batt_path);
			return NULL;
		}
	}

	output = compose_battery(status, capacity, time);

	/* Free resources */
	free(dir_batt_path);
	free(time);

	return output;
}

/*
 * Like get_battery(), but without time data
 */
char *
get_battery_no_time(const char *battery_name)
{
	char *output = NULL;
	char *dir_batt_path;
	batt_status_t status;
	int capacity;

	/* Compose battery directory */
	if (!(dir_batt_path = calloc(strlen(battery_name) + 25, sizeof(char))))
	{
		perror("Can't allocate battery info");
		return NULL;
	}
	sprintf(dir_batt_path, "/sys/class/power_supply/%s", battery_name);

	/* If battery directory doesn't exist, store in output "-" */
	if (!dir_readable(dir_batt_path))
	{
		if (!(output = strdup("-")))
			perror("Can't allocate battery info");
		free(dir_batt_path);
		return output;
	}

	/* Get battery status */
	status = read_battery_status(dir_batt_path);

	/* Get battery capacity */
	if ((capacity = read_capacity(dir_batt_path)) == -1)
	{
		free(dir_batt_path);
		return NULL;
	}

	output = compose_battery(status, capacity, NULL);

	/* Free resources */
	free(dir_batt_path);

	return output;
}
