![Skcolbmwd example](screenshot.png)

# Skcolbmwd

Skcolbmwd is a statusbar generator for the DWM and Tmux bars written and configured in C that uses "blocks" with similar options to `i3blocks`.

### Configuration

The configuration of Skcolbmwd is set in the `config.h` file in a similar way to DWM's config. To create the `config.h` file just run `make config.h`

Inside this file you'll find the character to use as separator and the `blocks` array. Each entry will be a block displayed in the bar in sequencial order. Each block allows to set a label, the update delay, a signal and if it should display a separator.

The following block types are available:
- *COMMAND*: get output from a shell command
- *BRIGHTNESS*: get backlight brightness
- *LOAD*: get system load averages
- *USED_RAM*: amount of used RAM
- *AVAILABLE_DISK*: get free disk space
- *NET_SPEED*: get download and upload speed of a network interface
- *BATTERY*: get battery percentage, status and time
- *TIME*: get formatted date

You can set as many blocks as you want in the `blocks` array. You have further explanations on each option inside the `config.h` file.

### CLI arguments
- `--tmux`: to set generated status as Tmux `status-right` instead of setting it in the DWM bar.
- `--stdout`: to output generated status to stdout instead of setting it in the DWM bar. Useful for testing/debugging

### Build

#### Dependencies
- Make
- C compiler
- libX11

#### Compilation
Once you have configured your `config.h`, just run `make`. If you want debug symbols, run `make debug`

#### Installation
You can directly run the compiled executable, but if you want to install it on your system, `make install` will do it. Notice that you can set the variable `PREFIX` (default to /usr/local) to your desire

#### Uninstall
`make uninstall` (Mind the `PREFIX` too)

### FAQ
- **Why?**
I'm aware there must be thousands of programs that do the same as this one, maybe better. Honestly I don't even know. I wrote Skcolbmwd because coming from i3 I wanted a similar program to i3blocks and it seemed like a fun program to write.
- **What does the name mean?**
`echo skcolbmwd | rev`
