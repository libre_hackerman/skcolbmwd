/*
 * Copyright (C) 2023 Esteban López Rodríguez <gnu_stallman@protonmail.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef SKCOLBMWD_H
#define SKCOLBMWD_H

#include <util.h>
#include <time.h>

typedef enum {COMMAND, BRIGHTNESS, LOAD, USED_RAM, AVAILABLE_DISK, NET_SPEED,
				BATTERY, BATTERY_NO_TIME, TIME} block_type_t;

typedef struct
{
	block_type_t type;
	char         *argument;
	char         *label;
	unsigned int delay;
	int          signal;
	bool         separator;
}
block_t;

typedef struct
{
	time_t  last_updated;
	bool    signal_recv;
}
block_status_t;

char *
get_command(const char *command);

char *
get_time(const char *format);

char *
get_used_ram();

char *
get_battery(const char *battery_name);
char *
get_battery_no_time(const char *battery_name);

char *
get_load(const char *n_avgs);

char *
get_available_disk(const char *mnt);

char *
get_net_speed(char *const *net_interfaces);

char *
get_brightness(const char *backlight);

#endif
