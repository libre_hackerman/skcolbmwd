/*
 * Copyright (C) 2023 Esteban López Rodríguez <gnu_stallman@protonmail.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef CONFIG_H
#define CONFIG_H

/* Character to print after each block, if enabled (see below) */
static const char separator = '|';

/*
 * Fields descriptions:
 * - TYPE: One of: COMMAND, BRIGHTNESS, LOAD, USED_RAM, AVAILABLE_DISK,
 *   NET_SPEED (set only ONCE if any), BATTERY, TIME
 * - ARGUMENT: Extra information for the block:
 *   - COMMAND: the shell command to execute.
 *   - BRIGHTNESS: entry in /sys/class/backlight/
 *   - LOAD: number of load averages (from 1 to 3)
 *   - AVAILABLE_DISK: mountpoint of the disk
 *   - NET_SPEED: space-separated list of interfaces names that will be checked.
 *   - BATTERY: the name of the battery in /sys/class/power_supply/
 *   - BATTERY_NO_TIME: like BATTERY, but showing only the percentage
 *   - TIME: the date formatting string.
 * - LABEL: text to precede the block with.
 * - DELAY: seconds between each update of the block. Set 0 to only update once.
 * - SIGNAL: update when receiving SIGRTMIN + the number specified. Set -1 to set none.
 * - SEPARATOR: true or false, to print or not the separator after the block.
 */
static block_t blocks[] = {
   /* TYPE,          ARGUMENT,          LABEL   DELAY, SIGNAL, SEPARATOR */
	{COMMAND,        "uname -r",        "K",    0,     -1,     true},
	{BRIGHTNESS,     "intel_backlight", "BR",   0,      1,     true},
	{LOAD,           "2",               "L",    5,     -1,     true},
	{USED_RAM,       NULL,              "MEM",  10,    -1,     true},
	{AVAILABLE_DISK, "/home",           "HOME", 30,    -1,     true},
	{NET_SPEED,      "wlan0 eno0",      "",     5,     -1,     true},
	{BATTERY,        "BAT0",            "BAT",  30,    -1,     true},
	{TIME,           "%H:%M %a %d-%m",  "",     30,    -1,     false},
};

#endif
